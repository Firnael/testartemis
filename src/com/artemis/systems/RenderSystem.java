package com.artemis.systems;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.components.SpatialForm;
import com.artemis.components.Transform;
import com.artemis.spatial.PlayerShip;
import com.artemis.spatial.Spatial;
import com.artemis.utils.Bag;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

/**
 * Created with IntelliJ IDEA.
 * User: anthonymarques
 * Date: 09/11/12
 * Time: 19:56
 * To change this template use File | Settings | File Templates.
 */
public class RenderSystem extends EntityProcessingSystem {

    private GameContainer container;
    private Graphics graphics;
    private Bag<Spatial> spatials;
    private ComponentMapper<SpatialForm> spatialFormMapper;
    private ComponentMapper<Transform> transformMapper;
    private CameraSystem cameraSystem;


    public RenderSystem(GameContainer container) {

        super(Aspect.getAspectForAll(Transform.class, SpatialForm.class));
        this.container = container;
        this.graphics = container.getGraphics();

        spatials = new Bag<Spatial>();
    }

    @Override
    public void initialize() {
        spatialFormMapper = world.getMapper(SpatialForm.class);
        transformMapper = world.getMapper(Transform.class);
        cameraSystem = world.getSystem(CameraSystem.class);
    }

    @Override
    protected void begin() {
        graphics.translate(-cameraSystem.getStartX(), -cameraSystem.getStartY());
    }

    @Override
    protected void process(Entity e) {

        Spatial spatial = spatials.get(e.getId());
        Transform transform = transformMapper.get(e);
        spatial.render(graphics);
    }

    @Override
    protected void end() {

        graphics.resetTransform();
        graphics.setColor(Color.white);
        graphics.drawString("Active entities: " + world.getEntityManager().getActiveEntityCount(), 10, 25);
        graphics.drawString("Total added to world since start: " + world.getEntityManager().getTotalAdded(), 10, 40);
        graphics.drawString("Total created in world since start: " + world.getEntityManager().getTotalCreated(), 10, 55);
        graphics.drawString("Total deleted from world since start: " + world.getEntityManager().getTotalDeleted(), 10, 70);
    }

    @Override
    protected void inserted(Entity e) {

        Spatial spatial = createSpatial(e);
        if (spatial != null) {
            spatial.initialize();
            spatials.set(e.getId(), spatial);
        }
    }

    @Override
    protected void removed(Entity e) {
        spatials.set(e.getId(), null);
    }

    private Spatial createSpatial(Entity e) {

        SpatialForm spatialForm = spatialFormMapper.get(e);
        String spatialFormFile = spatialForm.getSpatialFormFile();

        if ("PlayerShip".equalsIgnoreCase(spatialFormFile)) {
            return new PlayerShip(world, e);
        }

        return null;
    }
}
