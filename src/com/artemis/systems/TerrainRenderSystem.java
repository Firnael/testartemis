package com.artemis.systems;

import com.artemis.Aspect;
import com.artemis.Entity;
import com.artemis.EntitySystem;
import com.artemis.utils.ImmutableBag;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

/**
 * Created with IntelliJ IDEA.
 * User: Nael
 * Date: 14/11/12
 * Time: 17:07
 * To change this template use File | Settings | File Templates.
 */
public class TerrainRenderSystem extends EntitySystem {

    private GameContainer gc;
    private Graphics graphics;
    private Image tile;
    private CameraSystem cs;


    public TerrainRenderSystem(GameContainer gc) {
        super(Aspect.getEmpty());
        this.gc = gc;
        this.graphics = gc.getGraphics();
    }

    @Override
    protected void initialize() {

        try {
            tile = new Image("data/tile.png");
        } catch (SlickException e) {
            System.out.println("Can't load tile.png");
        }

        cs = world.getSystem(CameraSystem.class);
    }

    @Override
    protected void processEntities(ImmutableBag<Entity> entities) {

        float offsetX = cs.getStartX() % tile.getWidth();
        float offsetY = cs.getStartY() % tile.getHeight();

        int tilesWidth = (int) Math.ceil(cs.getWidth() / tile.getWidth()) + 1;
        int tilesHeight = (int) Math.ceil(cs.getHeight() / tile.getHeight()) + 1;

        graphics.translate(-offsetX, -offsetY);

        for (int x = -1; x < tilesWidth; x++) {
            for (int y = -1; y < tilesHeight; y++) {
                tile.draw(x * tile.getWidth(), y * tile.getHeight());
            }
        }

        graphics.resetTransform();
    }

    @Override
    protected boolean checkProcessing() {
        return true;
    }
}
