package com.artemis.systems;

import com.artemis.Aspect;
import com.artemis.Entity;
import com.artemis.EntitySystem;
import com.artemis.components.Transform;
import com.artemis.managers.TagManager;
import com.artemis.utils.ImmutableBag;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Input;
import org.newdawn.slick.MouseListener;

/**
 * Created with IntelliJ IDEA.
 * User: Nael
 * Date: 14/11/12
 * Time: 17:10
 * To change this template use File | Settings | File Templates.
 */
public class CameraSystem extends EntitySystem implements MouseListener {

    private GameContainer gc;
    private Entity player;
    private float lookAtX;
    private float lookAtY;
    private int screenWidth;
    private int screenHeight;
    private Input input;


    public CameraSystem(GameContainer gc) {
        super(Aspect.getEmpty());
        this.gc = gc;
    }

    @Override
    protected void initialize() {

        input = gc.getInput();
        input.addMouseListener(this);

        screenWidth = gc.getWidth();
        screenHeight = gc.getHeight();
    }

    @Override
    protected void processEntities(ImmutableBag<Entity> entityImmutableBag) {

        ensurePlayerEntity();

        if (player != null) {
            updatePosition();
            input.setOffset(getStartX(), getStartY());
        }
    }

    private void updatePosition() {

        Entity player = world.getManager(TagManager.class).getEntity("PLAYER");

        lookAtX = player.getComponent(Transform.class).getX();
        lookAtY = player.getComponent(Transform.class).getY();
    }

    @Override
    protected boolean checkProcessing() {
        return true;
    }

    private void ensurePlayerEntity() {

        if (player == null || !player.isActive()) {
            player = world.getManager(TagManager.class).getEntity("PLAYER");
        }
    }

    private float getOffsetX() {
        return ((float) screenWidth) / 2f;
    }

    private float getOffsetY() {
        return ((float) screenHeight) / 2f;
    }

    public float getStartX() {
        return lookAtX - getOffsetX();
    }

    public float getStartY() {
        return lookAtY - getOffsetY();
    }

    public float getEndX() {
        return lookAtX + getOffsetX();
    }

    public float getEndY() {
        return lookAtY + getOffsetY();
    }

    public float getWidth() {
        return getEndX() - getStartX();
    }

    public float getHeight() {
        return getEndY() - getStartY();
    }


    // === MouseListener implementation

    @Override
    public void mouseWheelMoved(int change) {
    }

    @Override
    public void mouseClicked(int button, int x, int y, int clickCount) {
    }

    @Override
    public void mousePressed(int button, int x, int y) {
    }

    @Override
    public void mouseReleased(int button, int x, int y) {
    }

    @Override
    public void mouseMoved(int oldx, int oldy, int newx, int newy) {
    }

    @Override
    public void mouseDragged(int oldx, int oldy, int newx, int newy) {
    }

    @Override
    public void setInput(Input input) {
    }

    @Override
    public boolean isAcceptingInput() {
        return true;
    }

    @Override
    public void inputEnded() {
    }

    @Override
    public void inputStarted() {
    }


}
