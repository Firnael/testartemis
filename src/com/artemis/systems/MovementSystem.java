package com.artemis.systems;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.components.Transform;
import com.artemis.components.Velocity;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

/**
 * Created with IntelliJ IDEA.
 * User: anthonymarques
 * Date: 09/11/12
 * Time: 20:19
 * To change this template use File | Settings | File Templates.
 */
public class MovementSystem extends EntityProcessingSystem {

    private GameContainer container;
    private Graphics g;
    private ComponentMapper<Velocity> velocityMapper;
    private ComponentMapper<Transform> transformMapper;


    public MovementSystem(GameContainer container) {
        super(Aspect.getAspectForAll(Transform.class, Velocity.class));
        this.container = container;
    }

    @Override
    public void initialize() {
        velocityMapper = world.getMapper(Velocity.class);
        transformMapper = world.getMapper(Transform.class);
    }

    @Override
    protected void process(Entity e) {

        Velocity v = velocityMapper.get(e);
        Transform t = transformMapper.get(e);

        t.addX(v.getVx() * world.getDelta());
        t.addY(v.getVy() * world.getDelta());
    }
}
