package com.artemis.components;

import com.artemis.Component;

/**
 * Created with IntelliJ IDEA.
 * User: anthonymarques
 * Date: 09/11/12
 * Time: 19:55
 * To change this template use File | Settings | File Templates.
 */
public class SpatialForm extends Component {
    private String spatialFormFile;

    public SpatialForm(String spatialFormFile) {
        this.spatialFormFile = spatialFormFile;
    }

    public String getSpatialFormFile() {
        return spatialFormFile;
    }
}
