package com.artemis.spatial;

import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.World;
import com.artemis.components.Transform;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Polygon;

/**
 * Created with IntelliJ IDEA.
 * User: anthonymarques
 * Date: 09/11/12
 * Time: 19:46
 * To change this template use File | Settings | File Templates.
 */
public class PlayerShip extends Spatial {

    private Transform transform;
    private Polygon ship;


    public PlayerShip(World world, Entity owner) {
        super(world, owner);
    }

    @Override
    public void initialize() {

        ComponentMapper<Transform> transformMapper = world.getMapper(Transform.class);
        transform = transformMapper.get(owner);

        ship = new Polygon();
        ship.addPoint(0, -10);
        ship.addPoint(10, 10);
        ship.addPoint(-10, 10);
        ship.setClosed(true);
    }

    @Override
    public void render(Graphics g) {

        g.setColor(Color.red);
        g.setAntiAlias(true);
        ship.setLocation(transform.getX(), transform.getY());
        g.fill(ship);
    }
}

