package com.artemis.spatial;

import com.artemis.Entity;
import com.artemis.World;
import org.newdawn.slick.Graphics;

/**
 * Created with IntelliJ IDEA.
 * User: anthonymarques
 * Date: 09/11/12
 * Time: 19:51
 * To change this template use File | Settings | File Templates.
 */
public abstract class Spatial {
    protected World world;
    protected Entity owner;

    public Spatial(World world, Entity owner) {
        this.world = world;
        this.owner = owner;
    }

    public abstract void initialize();

    public abstract void render(Graphics g);
}
