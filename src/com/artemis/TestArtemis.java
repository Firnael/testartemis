package com.artemis;

import com.artemis.components.SpatialForm;
import com.artemis.components.Transform;
import com.artemis.components.Velocity;
import com.artemis.managers.GroupManager;
import com.artemis.managers.TagManager;
import com.artemis.systems.*;
import org.newdawn.slick.*;

/**
 * Created with IntelliJ IDEA.
 * User: anthonymarques
 * Date: 09/11/12
 * Time: 19:31
 * To change this template use File | Settings | File Templates.
 */
public class TestArtemis extends BasicGame {

    private World world;
    private GameContainer container;
    private EntitySystem renderSystem;
    private EntitySystem terrainRenderSystem;

    public TestArtemis() {
        super("Test");
    }

    @Override
    public void init(GameContainer container) throws SlickException {
        this.container = container;

        world = new World();
        world.setManager(new GroupManager());
        world.setManager(new TagManager());

        renderSystem = world.setSystem(new RenderSystem(container), true);
        terrainRenderSystem = world.setSystem(new TerrainRenderSystem(container), true);

        world.setSystem(new MovementSystem(container));
        world.setSystem(new CameraSystem(container));
        world.setSystem(new PlayerShipControlSystem(container));

        world.initialize();

        initPlayerShip();
    }

    private void initPlayerShip() {
        Entity e = world.createEntity();
        e.addComponent(new Transform(container.getWidth() / 2, container.getHeight() - 50));
        e.addComponent(new SpatialForm("PlayerShip"));
        e.addComponent(new Velocity());

        world.getManager(GroupManager.class).add(e, "PLAYER_SHIP");
        world.getManager(TagManager.class).register("PLAYER", e);

        world.addEntity(e);
    }

    @Override
    public void update(GameContainer container, int delta) throws SlickException {
        world.setDelta(delta);
        world.process();
    }

    @Override
    public void render(GameContainer container, Graphics g) throws SlickException {

        terrainRenderSystem.process();
        renderSystem.process();
    }

    public static void main(String[] args) throws SlickException {

        TestArtemis game = new TestArtemis();
        AppGameContainer container = new AppGameContainer(game);
        container.setDisplayMode(800, 608, false);
        container.setAlwaysRender(true);
        // container.setMinimumLogicUpdateInterval(1);
        // container.setMaximumLogicUpdateInterval(1);
        // container.setTargetFrameRate(60);
        container.start();
    }
}
